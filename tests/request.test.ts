import { createServer, Server, IncomingMessage, ServerResponse, get, request } from 'http';

import * as assert from 'assert';
import { processRequest, Injectable, OnInit, OnInitPromise } from '../src/server/requestProcessor';
import { Route } from '../src/server/models/types';

const hostname = '127.0.0.1';
const port = 9999;

describe('requestProcessor', () => {
	it('processes a simple request', async () =>
		assert.equal(
			await execute(
				{},
				[
					{
						type: 'POST',
						keys: [],
						url: /url/,
						method() {
							return { response: 'foo' };
						},
					},
				],
				{}
			),
			'{"response":"foo"}',
			'processing simple request'
		));

	it('sends injectables to method', async () =>
		assert.equal(
			await execute(
				{},
				[
					{
						type: 'POST',
						keys: [],
						url: /url/,
						method(foo: string) {
							return { response: foo };
						},
					},
				],
				{ foo: 'bar' }
			),
			'{"response":"bar"}',
			'processing request with injectible'
		));

	it('sends OnInit injective to method', async () =>
		assert.equal(
			await execute(
				{},
				[
					{
						type: 'POST',
						keys: [],
						url: /url/,
						method(foo: string) {
							return { response: foo };
						},
					},
				],
				{ foo: new OnInit(() => 'bar') }
			),
			'{"response":"bar"}',
			'processing request with injectible'
		));

	it('sends remoteIp to method', async () =>
		assert.equal(
			await execute(
				{},
				[
					{
						type: 'POST',
						keys: [],
						url: /url/,
						method(remoteIp: string) {
							return { response: remoteIp };
						},
					},
				],
				{ foo: new OnInitPromise(async () => new Promise((resolve) => resolve('bar'))) }
			),
			'{"response":"127.0.0.1"}',
			'processing request with injectible'
		));

	it('sends referer to method', async () =>
		assert.equal(
			await execute(
				{},
				[
					{
						type: 'POST',
						keys: [],
						url: /url/,
						method(referer: string) {
							return { response: referer };
						},
					},
				],
				{}
			),
			`{"response":"http://${hostname}/referer"}`,
			'processing request with injectible'
		));

	it('sends language to method', async () =>
		assert.equal(
			await execute(
				{},
				[
					{
						type: 'POST',
						keys: [],
						url: /url/,
						method(language: string) {
							return { response: language };
						},
					},
				],
				{}
			),
			`{"response":"en"}`,
			'processing request with injectible'
		));

	it('sends post-data to method', async () =>
		assert.equal(
			await execute(
				{ foo: 'bar' },
				[
					{
						type: 'POST',
						keys: [],
						url: /url/,
						method(foo: string) {
							return { response: foo };
						},
					},
				],
				{}
			),
			`{"response":"bar"}`,
			'processing request with injectible'
		));

	it('handle two routes', async () =>
		assert.equal(
			await execute(
				{},
				[
					{
						type: 'POST',
						keys: [],
						url: /url/,
						method() {
							return { response: 1 };
						},
					},
					{
						type: 'POST',
						keys: [],
						url: /url/,
						method(response: number) {
							return { response2: response + 2 };
						},
					},
				],
				{}
			),
			'{"response":1,"response2":3}',
			'processing simple request'
		));
});

async function execute(data: any, routes: Route[], injectibles: { [key: string]: Injectable }): Promise<any> {
	const server = createServer(async (request: IncomingMessage, response: ServerResponse) =>
		response.end(JSON.stringify(await processRequest(routes, request, response, injectibles)))
	);
	server.listen(port);
	const response = await callServer(data);
	server.close();
	return response;
}

async function callServer(data: any): Promise<any> {
	const stringifiedData = JSON.stringify(data);
	const options = {
		hostname,
		port,
		path: '/',
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Content-Length': stringifiedData.length,
			'x-forwarded-for': `127.0.0.1`,
			referer: `http://${hostname}/referer`,
		},
	};

	return new Promise((resolve, reject) => {
		const req = request(options, (resp) => {
			let data = '';

			// A chunk of data has been recieved.
			resp.on('data', (chunk) => {
				data += chunk;
			});

			// The whole response has been received. Print out the result.
			resp.on('end', () => resolve(data));

			resp.on('error', (err) => reject('Error: ' + err.message));
		});

		req.on('error', (error) => reject(error));
		req.write(stringifiedData);
		req.end();
	});
}
