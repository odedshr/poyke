import * as assert from 'assert';
import { getRouter } from '../src/server/router';
import { RequestType, Route } from '../src/server/models/types';

describe('router', () => {
	it('returns a simple empty router', () => {
		const content = getRouter([])('GET', '').pop().method();
		assert.ok(content instanceof Error, 'returns an error');
		assert.equal(content.message, 'not-found', 'error with not-found message');
	});

	it('identifies different reques-types', () => {
		const types: RequestType[] = ['GET', 'POST', 'DELETE', 'PUT'];
		const basicRoute = { url: /url/, method: () => {} };
		const getRoute = getRouter(types.map((type: RequestType) => ({ type, ...basicRoute })));

		types.forEach((type) => assert.equal(getRoute(type, 'url').pop().type, type, 'return routes of the right type'));
	});

	it('returns not-found when failed to identify type', () => {
		const getRoute = getRouter([
			{
				type: 'GET',
				url: /url/,
				method: () => {},
			},
		]);
		// request identifier is an enum, so to send bad data (which might happen in real time) we must ts-ignore
		//@ts-ignore
		const content = getRoute('get', 'url').pop().method();
		assert.ok(content instanceof Error, 'returns an error');
		assert.equal(content.message, 'not-found', 'bad type results with not-found message');
	});

	it('returns multiple routes in the right order: no-format, json, html', () => {
		const type: RequestType = 'GET';
		const basicRoute = { type, url: /url/ };
		const getRoute = getRouter([
			{ ...basicRoute, format: 'application/json', method: () => 2 },
			{ ...basicRoute, method: () => 1 },
			{ ...basicRoute, format: 'text/html', method: () => 3 },
		]);
		const result = getRoute('GET', 'url')
			.map((route) => route.method())
			.join();
		assert.equal(result, '1,2,3', 'ran multiple routes in the right order');
	});

	it('returns multiple routes with short url first', () => {
		const type: RequestType = 'GET';
		const getRoute = getRouter([
			{ type, url: /url1/, method: () => 2 },
			{ type, url: /url/, method: () => 1 },
			{ type, url: /url12/, method: () => 3 },
		]);
		const result = getRoute('GET', 'url12')
			.map((route) => route.method())
			.join();
		assert.equal(result, '1,2,3', 'ran multiple routes in the right order');
	});

	it('parse keys from url', () => {
		const url = /\/signin\/{{token:integer}}\/{{email}}\/?$/;
		const result = getRouter([
			{
				type: 'GET',
				url,
				method: () => {},
			},
		])('GET', '/signin/1234/email@domain.com').pop();

		assert.equal(JSON.stringify(result.keys), '["token","email"]', 'return expected tokens');
		assert.equal(
			result.url,
			/\/signin\/(\d+)\/((([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})))\/?$/.toString(),
			'transpile url to a standard regexp'
		);
		assert.equal(url, result.original, 'keeps original URL');
	});

	it('parse argument:integer', () => {
		const url = /\/foo\/{{test:integer}}\/?$/;
		const getRoute = getRouter([
			{
				type: 'GET',
				url,
				method: () => {},
			},
		]);

		assert.equal(getRoute('GET', '/foo/1234/').pop().original, url, 'find route with a number');
		assert.ok(getRoute('GET', '/foo/xx/').pop().method() instanceof Error, 'route without number is mismatched');
	});

	it('parse argument:string', () => {
		const url = /\/foo\/{{test:string}}\/?$/;
		const getRoute = getRouter([
			{
				type: 'GET',
				url,
				method: () => {},
			},
		]);

		assert.equal(getRoute('GET', '/foo/bar/').pop().original, url, 'find route with a string');
	});

	it('parse argument:email', () => {
		const url = /\/foo\/{{test:email}}\/?$/;
		const getRoute = getRouter([
			{
				type: 'GET',
				url,
				method: () => {},
			},
		]);

		assert.equal(getRoute('GET', '/foo/email@domain.suffix/').pop().original, url, 'find route with a email');
		assert.ok(getRoute('GET', '/foo/xx/').pop().method() instanceof Error, 'route without email is mismatched');
	});

	it('parse argument:id', () => {
		const url = /\/foo\/{{id}}\/?$/;
		const getRoute = getRouter([
			{
				type: 'GET',
				url,
				method: () => {},
			},
		]);

		assert.equal(getRoute('GET', '/foo/asdf-12aa-2353-fds2/').pop().original, url, 'find route with a id');
		assert.ok(getRoute('GET', '/foo/x@x/').pop().method() instanceof Error, 'route without id is mismatched');
	});

	it('parse unknown url argument', () => {
		const url = /\/foo\/{{test:foo}}\/?$/;
		assert.throws(
			() =>
				getRouter([
					{
						type: 'GET',
						url,
						method: () => {},
					},
				]),
			'not-found'
		);
	});
});
