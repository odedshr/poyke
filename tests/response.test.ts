import { createServer, Server, IncomingMessage, ServerResponse, get } from 'http';

import * as assert from 'assert';
import { write, ResponseData } from '../src/server/responseWriter';
import { DetailedError } from '../src/shared/Errors';
import { responseStatus } from '../src/server/models/types';
import { readFileSync, readdirSync } from 'fs';

const PORT = 9999;

describe('responseWriter', () => {
	it('processes a empty request', async () =>
		assert.equal(await processData(undefined, getResponseContent), '', 'simple empty response'));

	it('processes a simple request', async () =>
		assert.ok(
			(await processData({}, getResponseContent)).match(/{"processTime":0.0+\d}/),
			'return value includes processTime'
		));

	it('processes a simple request with array', async () =>
		assert.equal(
			JSON.parse(await processData({ content: [1, 2, 3] }, getResponseContent)).contentLength,
			3,
			'return value includes content count size'
		));

	it('processes a simple request with object', async () =>
		assert.equal(
			JSON.parse(await processData({ content: { foo: 'a', bar: 'b' } }, getResponseContent)).contentSize,
			2,
			'return value includes content count size'
		));

	it('processes custom-data', async () => {
		assert.equal(
			Object.keys(
				//@ts-ignore
				JSON.parse(await processData({ foo: 'x', fun: { a: 1, b: 2 }, games: [1, 2, 3] }, getResponseContent))
			).join(),
			'foo,fun,games,funSize,gamesLength,processTime',
			'return all keys'
		);
	});

	it('return the right status', async () => {
		assert.equal(
			(await processData({ content: [1, 2] }, getResponse)).statusCode,
			responseStatus.ok,
			'returns ok by default'
		);
		assert.equal(
			(await processData({ status: responseStatus.notFound }, getResponse)).statusCode,
			responseStatus.notFound,
			'return notFound'
		);
		assert.equal(
			(await processData({ status: responseStatus.redirect }, getResponse)).statusCode,
			responseStatus.redirect,
			'return redirect'
		);
		assert.equal(
			(await processData({ status: responseStatus.systemError }, getResponse)).statusCode,
			responseStatus.systemError,
			'return systemError'
		);
	});

	it('processes a redirect', async () =>
		assert.equal(
			(await processData({ _redirect: '/redirect' }, getResponse)).statusCode,
			responseStatus.redirect,
			'returns status redirected'
		));

	it('processes different output types', async () => {
		assert.equal(
			await processData({ _type: 'text/html', content: '<html><body>hello world</body></html>' }, getResponseContent),
			'<html><body>hello world</body></html>',
			'returns html content'
		);

		assert.equal(
			(await processData({ _type: 'text/html', content: '<html><body>hello world</body></html>' }, getResponse))
				.headers['content-type'],
			'text/html',
			'returns html content'
		);

		assert.equal(
			(await processData({ _type: 'text/svg', content: '<svg></svg>' }, getResponse)).headers['content-type'],
			'text/svg',
			'returns svg content'
		);

		assert.equal(
			(await processData({ _type: 'binary', content: readFileSync('./tests/testFile.ico') }, getResponseContent))
				.length,
			318,
			'downloads a file'
		);
	});
});

async function processData(data: ResponseData | DetailedError, handlerFn: () => Promise<any>) {
	const startTime = new Date();
	const server = createServer(async (request: IncomingMessage, response: ServerResponse) =>
		write(response, startTime, data)
	);
	server.listen(PORT);
	const content: any = await handlerFn();

	server.close();
	return content;
}

async function getResponse(): Promise<IncomingMessage> {
	return new Promise((resolve) => get(`http://127.0.0.1:${PORT}`, (resp) => resolve(resp)));
}

async function getResponseContent(): Promise<any> {
	return new Promise((resolve) =>
		get(`http://127.0.0.1:${PORT}`, (resp) => {
			let data = '';

			// A chunk of data has been recieved.
			resp.on('data', (chunk) => {
				data += chunk;
			});

			// The whole response has been received. Print out the result.
			resp.on('end', () => {
				resolve(data);
			});

			resp.on('error', (err) => {
				console.log('Error: ' + err.message);
			});
		})
	).catch((err) => console.error('getResponseContent:', err));
}
