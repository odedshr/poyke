CREATE TABLE "users" (
	"rowid"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"id"	BLOB NOT NULL UNIQUE,
	"email"	INTEGER NOT NULL UNIQUE,
	"defaultAlias"	TEXT,
	"created" INTEGER CURRENT_TIMESTAMP
);

CREATE TABLE "groups" (
	"rowid"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"id"	BLOB NOT NULL UNIQUE,
	"name"	TEXT NOT NULL,
	"description"	TEXT,
	"status"	TEXT DEFAULT 'active',
	"type"	TEXT DEFAULT 'secret',
	"founderId"	BLOB NOT NULL,
	"hasImage"	INTEGER NOT NULL DEFAULT 0,
	"isAnonymousAllowed"	INTEGER DEFAULT 0,
	"postLength"	INTEGER,
	"minAge"	INTEGER,
	"maxAge"	INTEGER,
	"gender"	TEXT,
	"members"	INTEGER DEFAULT 1,
	"posts"	INTEGER DEFAULT 0,
	"events"	INTEGER,
	"score"	INTEGER DEFAULT 0,
	"scoreDate"	TEXT,
	"created" INTEGER CURRENT_TIMESTAMP,
	"modified"	INTEGER CURRENT_TIMESTAMP,
	FOREIGN KEY("founderId") REFERENCES "users"("id") ON UPDATE CASCADE
);

CREATE TABLE "memberships" (
	"row"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"id"	BLOB NOT NULL UNIQUE,
	"userId"	BLOB NOT NULL,
	"groupId"	BLOB NOT NULL,
	"alias"	TEXT,
	"status"	TEXT DEFAULT 'active',
	"description"	TEXT,
	"hasImage"	INTEGER,
	"birthDate"	TEXT,
	"permissions"	BLOB,
	"penalties"	BLOB,
	"isModerator"	INTEGER DEFAULT 0,
	"isPublic"	INTEGER DEFAULT 0,
	"isRepresentative"	INTEGER DEFAULT 0,
	"created" INTEGER CURRENT_TIMESTAMP,
	"modified"	INTEGER CURRENT_TIMESTAMP,
	FOREIGN KEY("userId") REFERENCES "users"("id") ON UPDATE CASCADE
	FOREIGN KEY("groupId") REFERENCES "groups"("id") ON UPDATE CASCADE
);