import { Route } from './models/types';
import authenticationRoutes from './authentication/authentication.routes';
import homeRoutes from './home/home.routes';
import groupRoutes from './group/group.routes';
import userRoutes from './user/user.routes';

export const routes: Route[] = [
	{
		type: 'GET',
		url: /\/ping/,
		method() {
			return { response: 'pong' };
		},
	},
	{
		type: 'GET',
		url: /\/test-db/,
		async method(test: () => void) {
			console.log('11');
			test();
			console.log('2');
			return { response: 'tested' };
		},
	},
	{
		type: 'GET',
		url: /\/test\/{{email}}/,
		async method(email: string, authUser: any) {
			console.log('email:', email, authUser);
			return { response: 'tested' };
		},
	},
	...authenticationRoutes,
	...groupRoutes,
	...userRoutes,
	...homeRoutes,
];
