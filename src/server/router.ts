import { emailPattern, maskedIdPattern } from './validations';
import { BadInput, NotFound } from '../shared/Errors';
import { RequestType, Route } from './models/types';

const urlParamPattern = new RegExp('\\{\\{([^#]+?)\\}\\}', 'g');

export interface AnalysedRoute extends Route {
	original: RegExp;
}

export type RouterFunction = (requestType: RequestType, url: string) => AnalysedRoute[];

export function getRouter(routes: Route[]): RouterFunction {
	const AnalysedRoutes: { [key: string]: AnalysedRoute[] } = {
		GET: [],
		POST: [],
		PUT: [],
		DELETE: [],
	};

	routes.forEach((route: Route) => AnalysedRoutes[route.type].push(analyseRoute(route)));
	Object.keys(AnalysedRoutes).forEach((key) => AnalysedRoutes[key].sort(sortRoutes));

	return (type: RequestType, url: string) => {
		const matchingRoutes = AnalysedRoutes[type]?.filter((route: Route) => url.match(route.url));
		return matchingRoutes && matchingRoutes.length ? matchingRoutes : getNotFoundRoutes(type, url);
	};
}

function getNotFoundRoutes(type: RequestType, url: string): AnalysedRoute[] {
	const regExpUrl = new RegExp(url);
	return [
		{
			type,
			url: regExpUrl,
			original: regExpUrl,
			method() {
				return new NotFound('route', 'url');
			},
		},
	];
}

function sortRoutes(routeA: Route, routeB: Route): number {
	// (if A is undefined) or (B is defined but A is json)
	if ((!routeA.format && routeB.format) || (!routeB.format && routeA.format === 'application/json')) {
		return -1;
	}

	// (if B is undefined) or (A is html AND B is json)
	if ((!routeB.format && routeA.format) || (routeA.format === 'text/html' && routeB.format === 'application/json')) {
		return 1;
	}

	// otherwise they're equal, take short URLs first (so /user/ will be read before /user\/123/)
	return routeA.url.toString().length - routeB.url.toString().length;
}

function analyseRoute(route: Route): AnalysedRoute {
	const { keys, url } = getArgumentNamesFromUrlTempalte(route.url);

	return {
		...route,
		original: route.url,
		keys,
		url,
	};
}

function getArgumentNamesFromUrlTempalte(urlTemplate: RegExp): { keys: string[]; url: RegExp } {
	const keys: string[] = [];
	let urlString: string = urlTemplate.source;
	let item: any;

	while ((item = urlParamPattern.exec(urlString)) !== null) {
		// item[0] = {{key:type}}, item[1] = key:type
		const [key, type] = item[1].split(':');

		keys.push(key);

		try {
			urlString = urlString.split(item[0]).join(getPatternByType(type || key));
		} catch (err) {
			// console.debug(err);
			throw new BadInput(urlTemplate.source, err);
		}

		urlParamPattern.lastIndex = 0;
	}

	return { keys, url: new RegExp(urlString) };
}

function getPatternByType(type: string) {
	switch (type) {
		case 'id':
			return maskedIdPattern;
		case 'email':
			return emailPattern;
		case 'string':
			return '(.+)';
		case 'integer':
			return '(\\d+)';
		default:
			throw new NotFound('url-param', type);
	}
}
