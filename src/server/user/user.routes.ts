import { Route } from '../models/types';
import { setUser, getUser } from './user.api';
import { redirect } from '../http-utils';

const routes: Route[] = [
	{
		type: 'POST',
		url: /\/user/,
		method: setUser,
	},
	{
		type: 'POST',
		url: /\/user/,
		format: 'text/html',
		method: redirect.backToReferer,
	},
	{
		type: 'GET',
		url: /\/user/,
		method: getUser,
	},
	{
		type: 'GET',
		url: /\/user/,
		format: 'text/html',
		method: redirect.backToReferer,
	},
];

export default routes;
