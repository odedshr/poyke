import { DOMParser } from 'xmldom';
import SignEmailSentTemplate from './signinEmailSent.template';
import { Dictionary } from '../models/export';

export async function displaySigninEmailSent(content: any): Promise<Dictionary> {
	return new Promise((resolve) => resolve({ _string: new SignEmailSentTemplate({}, new DOMParser()).toString() }));
}
