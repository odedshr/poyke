import { ModelSpec, Variables } from '../models/export';
import { User, userSpec } from '../models/User';
import { NotFound } from '../../shared/Errors';

export async function getUser(
	authUser: string,
	getById: (modelSpec: ModelSpec, id: string) => Promise<User>
): Promise<{ user: User }> {
	const user = await getById(userSpec, authUser);
	if (!user) {
		throw new NotFound('user', authUser);
	}

	return { user };
}

export async function setUser(
	authUser: string,
	user: User = {} as User,
	defaultAlias: string,
	getSetted: (dataObject: Variables, modelSpec: ModelSpec) => Promise<User>
) {
	user = Object.assign(user, { id: authUser, defaultAlias });
	return { user: await getSetted(<Variables>(<unknown>user), userSpec) };
}
