import { DOMParser } from 'xmldom';
import { Dictionary } from '../models/types';
import CommonPage from '../pages/common.template';
import HomePageAnonymous from './home-anonymous.template';
import HomePageCustom from './home-custom.template';
import AddGroupForm from '../group/group-add-form.template';
import GroupList from '../group/group-list.template';
import UserInfo from '../user/user-info.template';
import { Group, ModelSpec } from '../models/export';
import { getGroups } from '../group/group.api';
import { getUser } from '../user/user.api';
import { getString } from '../../shared/i18n';

const domParser = new DOMParser();

async function getAnonymousHomepage(language: string): Promise<Dictionary> {
	return new Promise<Dictionary>((resolve) =>
		resolve({
			_string: new CommonPage(
				{ title: getString('appTitle', language), content: new HomePageAnonymous({}, domParser) },
				domParser
			).toString(),
		})
	);
}

async function getCustomisedHomepage(
	userId: string,
	language: string,
	getById: (modelSpec: ModelSpec, id: string) => Promise<any>,
	get: (query: string) => Promise<any>
): Promise<Dictionary> {
	const groups: Group[] = (await getGroups(userId, get)).response.map((group: Group) => ({
		...group,
		link: `/group/${group.id}`,
	}));
	const { user } = await getUser(userId, getById);

	return new Promise<Dictionary>((resolve) =>
		resolve({
			_string: new CommonPage(
				{
					title: getString('appTitle', language),
					content: new HomePageCustom(
						{
							userId,
							addGroupForm: user.defaultAlias
								? new AddGroupForm(
										{
											strings: {
												addGroupTitle: getString('addGroupTitle', language),
												submit: getString('addGroupCTA', language),
											},
										},
										domParser
								  )
								: false,
							userBox: new UserInfo({ user }, domParser),
							groups: new GroupList({ groups }, domParser),
						},
						domParser
					),
				},
				domParser
			).toString(),
		})
	);
}

export async function getHomepage(
	optionalUser: string,
	get: (query: string) => Promise<any>,
	getById: (modelSpec: ModelSpec, id: string) => Promise<any>,
	language: string
): Promise<Dictionary> {
	return await (optionalUser
		? getCustomisedHomepage(optionalUser, language, getById, get)
		: getAnonymousHomepage(language));
}
