import { Route } from '../models/types';
import { getHomepage } from './home.ui';

const routes: Route[] = [
	{
		type: 'GET',
		url: /\//,
		format: 'text/html',
		method: getHomepage,
	},
];

export default routes;
