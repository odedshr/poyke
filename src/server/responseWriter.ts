import { ServerResponse } from 'http';
import { DetailedError } from '../shared/Errors';
import { Dictionary, responseStatus } from './models/types';

export type ResponseType = 'text/html' | 'text/svg' | 'application/json' | 'binary';

export interface ResponseData {
	status?: number;
	_type?: ResponseType;
	content?: any;
	_redirect?: string;
	_headers?: Dictionary;
	processTime?: number;
}

export function write(response: ServerResponse, startTime: Date, data: ResponseData | DetailedError) {
	if (data === undefined || data === null) {
		response.end();
		return;
	} else {
		if (data instanceof DetailedError) {
			data = getDetailedErrorResponseData(data);
		}

		const headers = data._headers ? data._headers : {};
		delete data._headers;
		if (!headers['Content-Type']) {
			headers['Content-Type'] = data._type || 'text/html';
		}

		response.writeHead(data.status || responseStatus.ok, headers);

		if (data._type && data._type.indexOf('text') === 0) {
			response.end(data.content);
		} else if (data._type && 'binary') {
			return response.end(data.content, 'binary');
		} else if (data._redirect !== undefined) {
			response.writeHead(data.status || responseStatus.redirect, { ...headers, Location: data._redirect });
			return response.end();
		} else if (typeof data !== 'object') {
			data = { content: data };
		}

		addObjectSizes(data);
		data.processTime = getSecondsSince(startTime);

		response.end(JSON.stringify(data));
	}
}

function getDetailedErrorResponseData(data: DetailedError): Dictionary {
	return {
		message: data.message,
		details: data.details,
		status: data.status || 500,
	};
}

function getSecondsSince(startTime: Date) {
	return (new Date().getTime() - startTime.getTime()) / 1000;
}

function addObjectSizes(data: { [key: string]: any }) {
	Object.keys(data).forEach((key) => {
		const value = data[key];

		if (value !== null && value !== undefined) {
			if (Array.isArray(data[key])) {
				data[`${key}Length`] = data[key].length;
			} else if (typeof data[key] === 'object' && key !== 'error') {
				data[`${key}Size`] = Object.keys(data[key]).length;
			}
		}
	});
}
