import { IncomingMessage } from 'http';

import { WebServer } from './WebServer';
import { Injectable, OnInitPromise, OnInit } from './requestProcessor';
import { DataProvider } from './data-provider';
import { getAuthToken, getAuthUser, getOptionalUser } from './authentication/authentication.api';
import { Variables } from './models/types';
import { routes } from './routes';
import { getRouter } from './router';

export async function init(prameters: Variables) {
	const dataProvider = new DataProvider('./dist/var/db.sql');
	await dataProvider.init('./src/sql');

	const injectables: { [key: string]: Injectable } = {
		authToken: new OnInit((req: IncomingMessage) => {
			return getAuthToken(req);
		}),

		authUser: new OnInitPromise(async (req: IncomingMessage) => {
			const data = await getAuthUser(req);
			return data;
		}),

		optionalUser: new OnInitPromise(async (req: IncomingMessage) => {
			return await getOptionalUser(req);
		}),

		...dataProvider.getInjectibles(),
	};

	const rootFolder = `${process.cwd()}/${process.env.npm_package_rootFolder}`;
	const defaultFile = 'index.html';

	let port = +prameters.PORT;
	const getRoute = getRouter(routes);
	const server: WebServer = new WebServer(port, rootFolder, defaultFile, getRoute, injectables);

	await server.start(port).catch((err) => console.error(err));
	console.log(`server running on port ${port}`);
}
