import { verbose as SQLite3, Database } from 'sqlite3';
import { readdirSync, readFileSync } from 'fs';
import { verifyFolderExists } from './file-utils';

import { User, Variables, Dictionary, ModelSpec, ColumnSpec } from './models/export';
import { BadInput, MissingInput } from '../shared/Errors';

const inMemoryDb = ':memory:';
const uuid = 'hex(randomblob(16))';
export class DataProvider {
	connectionStr: string;

	constructor(connectionStr: string) {
		this.connectionStr = connectionStr;

		if (connectionStr !== inMemoryDb) {
			verifyFolderExists(connectionStr.substr(0, connectionStr.lastIndexOf('/')));
		}
	}

	private getDB(): Database {
		const sqlite3 = SQLite3();
		return new sqlite3.Database(this.connectionStr);
	}

	async init(migrationFolder: string = inMemoryDb) {
		await this.withDb(async (db: Database) => await this.updateDB(db, migrationFolder));
	}

	private async updateDB(db: Database, migrationFolder: string) {
		const versionRecord = await getSingle(db, 'PRAGMA user_version');
		const version: string = '' + versionRecord.user_version;
		const migrationsFiles: string[] = readdirSync(migrationFolder)
			.filter((filename) => version < filename.match(/-((\d+\.?)*)\.sql/)[1])
			.sort();

		if (migrationsFiles.length) {
			const newVersion = migrationsFiles[migrationsFiles.length - 1].match(/-(\d+)\.sql/)[1];
			db.serialize(async () => {
				migrationsFiles.forEach(
					async (filename) => await exec(db, readFileSync(`${migrationFolder}/${filename}`, 'utf-8'))
				);
				await exec(db, `PRAGMA user_version=${newVersion}`);
			});

			console.info(`db migrated from v${version} to v${newVersion}`);
		} else {
			console.info(`db is up to date (v${version})`);
		}

		return {};
	}

	public async getUserIdByEmail(db: Database, email: string): Promise<string> {
		return new Promise(async (resolve) => {
			await exec(
				db,
				`INSERT INTO users(id, email) SELECT ${uuid}, '${email}' WHERE NOT EXISTS(SELECT 1 FROM users WHERE email = '${email}');`
			);
			const user: Partial<User> = await getSingle(db, `SELECT id FROM users WHERE email="${email}"`);
			resolve('' + user.id);
		});
	}

	public getInjectibles(): Dictionary {
		return {
			getUserIdByEmail: async (email: string) =>
				await this.withDb(async (db) => await this.getUserIdByEmail(db, email)),
			getById: async (modelSpec: ModelSpec, id: string) =>
				await this.withDb(async (db) => await getById(db, modelSpec, id)),
			getSingle: async (query: string) => await this.withDb(async (db) => await getSingle(db, query)),
			get: async (query: string) => await this.withDb(async (db) => await get(db, query)),
			exec: async (query: string) => await this.withDb(async (db) => await exec(db, query)),
			set: async (dataObject: Dictionary, modelSpec: ModelSpec) =>
				await this.withDb(async (db) => await (dataObject.id ? update : insert)(db, dataObject, modelSpec)),
			getSetted: async (dataObject: Dictionary, modelSpec: ModelSpec) =>
				await this.withDb(async (db) => await (dataObject.id ? getUpdated : getInserted)(db, dataObject, modelSpec)),
			insert: async (dataObject: Dictionary, modelSpec: ModelSpec) =>
				await this.withDb(async (db) => await insert(db, dataObject, modelSpec)),
			update: async (dataObject: Dictionary, modelSpec: ModelSpec) =>
				await this.withDb(async (db) => await update(db, dataObject, modelSpec)),
		};
	}

	async withDb(method: (db: Database) => any): Promise<any> {
		const db = this.getDB();
		const result = await method(db);
		db.close();
		return result;
	}
}

async function exec(db: Database, query: string): Promise<void> {
	return new Promise((resolve, reject) =>
		db.exec(query, (err: Error) => {
			if (err) {
				console.error(query, err);
				reject(err);
			}
			resolve();
		})
	);
}

async function get(db: Database, query: string): Promise<any> {
	return new Promise((resolve, reject) => {
		db.all(query, (err: Error, rows: any) => {
			if (err) {
				console.error(query, err);
				reject(err);
			} else {
				resolve(rows);
			}
		});
	});
}

async function getById(db: Database, modelSpec: ModelSpec, id: string) {
	return getSingle(db, `SELECT ${Object.keys(modelSpec.columns).join(',')} FROM ${modelSpec.table} WHERE id='${id}'`);
}

async function getSingle(db: Database, query: string): Promise<any> {
	return new Promise((resolve, reject) => {
		db.get(query, (err: Error, row: any) => {
			if (err) {
				console.error(query, err);
				reject(err);
			} else {
				resolve(row);
			}
		});
	});
}

function formatValue(key: string, value: any, column: ColumnSpec = {}) {
	// 'boolean' | 'datetime' | 'enum' | 'number' | 'string' | 'timestamp'
	if (value === undefined) {
		if (column.isRequired) {
			throw new MissingInput(key);
		}
		return null;
	}
	switch (column.type) {
		case 'boolean':
			return value ? 1 : 0;
		case 'datetime':
			return `"${value.toJSON().slice(0, 19).replace('T', ' ')}"`;
		case 'enum':
			if (column.values.indexOf(value) === -1) {
				throw new BadInput(key, value);
			}
			return `"${value}"`;
		case 'number':
			if (isNaN(value)) {
				if (column.values.indexOf(value) === -1) {
					throw new BadInput(key, value);
				}
			}
			return value;
		case 'string':
			return `"${value}"`;
		default:
			return value;
	}
}

async function getInserted(db: Database, dataObject: Variables, modelSpec: ModelSpec) {
	insert(db, dataObject, modelSpec);
	const data = await getSingle(db, `SELECT * FROM ${modelSpec.table} WHERE rowid=last_insert_rowid()`);
	return data;
}

async function insert(db: Database, dataObject: Variables, modelSpec: ModelSpec) {
	const keys = Object.keys(dataObject),
		values = keys.map((key) => `${formatValue(key, dataObject[key], modelSpec[key])}`),
		query = `INSERT INTO ${modelSpec.table} ("id", "${keys.join('","')}") VALUES (${uuid}, ${values.join(',')});`;
	await exec(db, query);
}

async function getUpdated(db: Database, dataObject: Variables, modelSpec: ModelSpec) {
	update(db, dataObject, modelSpec);
	return await getSingle(db, `SELECT * FROM ${modelSpec.table} WHERE id="${dataObject.id}"`);
}

async function update(db: Database, dataObject: Variables, modelSpec: ModelSpec) {
	const values: string[] = Object.keys(dataObject)
		.filter((key) => key !== 'id' && key !== 'rowid')
		.map((key) => `${key} = ${formatValue(key, dataObject[key], modelSpec.columns[key])}`);
	const query = `UPDATE ${modelSpec.table} SET ${values.join(',')} WHERE id="${dataObject.id}";`;

	await exec(db, query);
}

export async function test(db: Database): Promise<void> {
	return new Promise((resolve) => {
		db.serialize(async () => {
			// db.run('CREATE TABLE lorem (info TEXT)');
			console.log(await getSingle(db, 'PRAGMA user_version'));
			var stmt = db.prepare('INSERT INTO lorem VALUES (?)');
			for (var i = 0; i < 10; i++) {
				stmt.run('Ipsum ' + i);
			}
			stmt.finalize();

			db.each('SELECT rowid AS id, info FROM lorem', function (err, row) {
				console.log(row.id + ': ' + row.info);
			});
			resolve();
		});
	});
}
