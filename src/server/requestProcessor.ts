import { IncomingMessage, ServerResponse } from 'http';
import { TooLong, Unauthorized } from '../shared/Errors';
import { Route, Dictionary } from './models/types';
import { getBestlanguage } from '../shared/i18n';

export interface Injectable {}

export class OnInit {
	method: Function;

	constructor(method: Function) {
		this.method = method;
	}
}

export class OnInitPromise {
	method: Function;

	constructor(method: Function) {
		this.method = method;
	}
}

export async function processRequest(
	routes: Route[],
	request: IncomingMessage,
	response: ServerResponse,
	injectables: { [key: string]: Injectable } = {}
): Promise<Dictionary> {
	const injectablesAndParameters: Dictionary = Object.assign(
		{
			remoteIp: fixToIpV4(getRemoteIP(request)),
			referer: request.headers.referer,
			language: getBestlanguage(request.headers['accept-language']),
		},
		await getPostData(request).catch((err) => {
			console.error('error getting post data', err);
			return {};
		}),
		injectables
	);

	let results: { [key: string]: any } = {};
	for (let i in routes) {
		const result = await execute(
			routes[i].method,
			Object.assign(injectablesAndParameters, getParametersFromURL(request.url, routes[i]), results),
			request,
			response
		);

		results = Object.assign(results, result);
	}

	return results;
}

async function execute(
	method: Function,
	injectablesAndParameters: Dictionary,
	request: IncomingMessage,
	response: ServerResponse
): Promise<Dictionary> {
	const argumentNames: string[] = parseArgumentNamesFromFunctionName(method);
	const args: any[] = await getArgumentValues(argumentNames, injectablesAndParameters, request, response).catch(
		(err) => {
			console.error(`error fetching arguments for ${request.url}`, err, err instanceof Unauthorized);
			throw err;
		}
	);

	return await executeMethodWithArguments(method, args).catch((err) => {
		console.error(`error executing method for for ${request.url}`, err);
		throw err;
	});
}

function getRemoteIP(request: IncomingMessage): string {
	return forceToArray(
		request.headers['x-forwarded-for'] || request.connection.remoteAddress || request.socket.remoteAddress
	).join(', ');
}

function forceToArray(item: any) {
	return Array.isArray(item) ? item : [item];
}

function fixToIpV4(ip: string) {
	return ip.replace(/::ffff:/, '');
}

function getParametersFromURL(requestedUrl: string, route: Route): { [key: string]: any } {
	const output: { [key: string]: any } = {};
	const { keys, url } = route;
	let i = 0;

	url.lastIndex = 0;
	let values = url.exec(requestedUrl) || [];
	values.shift();
	const count = Math.min(keys.length, values.length);

	while (i < count) {
		output[keys[i]] = values[i++];
	}

	return output;
}

async function getPostData(request: IncomingMessage): Promise<{ [key: string]: any }> {
	const buffer: string[] = [];

	return new Promise((resolve, reject) => {
		if (request.method == 'POST') {
			request.on('data', (data) => {
				buffer.push(data);
				if (buffer.length > 1e6) {
					request.connection.destroy();
					reject(new TooLong('input too big', 1e6));
				}
			});

			request.on('end', () => resolve(parsePostData(buffer.join().trim())));
		} else {
			resolve({});
		}
	});
}

function parsePostData(data: string): { [key: string]: string } {
	if (data.length) {
		try {
			return JSON.parse(data);
		} catch (err) {
			const fields: { [key: string]: string } = {};
			new URLSearchParams(data).forEach((value: string, name: string) => {
				fields[name] = value;
			});
			return fields;
		}
	}
	return { data };
}

function getArgumentValues(
	argumentNames: string[],
	injectables: { [key: string]: Injectable },
	request: IncomingMessage,
	response: ServerResponse
): Promise<any[]> {
	return new Promise((resolve) => {
		const argumentValues = Promise.all(
			argumentNames.map(
				(argName: string) =>
					new Promise(async (resolveInjectible, rejectInjectible) => {
						const injectable = injectables[argName];

						try {
							if (injectable instanceof OnInit) {
								resolveInjectible(injectable.method(request, response));
							} else if (injectable instanceof OnInitPromise) {
								resolveInjectible(await injectable.method(request, response));
							} else {
								resolveInjectible(injectable);
							}
						} catch (err) {
							return rejectInjectible(err);
						}
					})
			)
		).catch((err) => {
			throw err;
		});

		resolve(argumentValues);
	});
}

async function executeMethodWithArguments(method: Function, argumentValues: any[]) {
	try {
		return await method(...argumentValues);
	} catch (err) {
		return err;
	}
}

function parseArgumentNamesFromFunctionName(method: Function): string[] {
	const items = method.toString().match(/^function\s?[^\(]+\s?\(([^\)]+)\)+/);

	return items === null ? [] : items[1].replace(/\s/g, '').split(',');
}
