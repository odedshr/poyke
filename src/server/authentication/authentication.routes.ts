import { sendSignInEmail, completeSignInEmail, signout } from './authentication.api';
import { displaySigninEmailSent } from './authentication.ui';
import { Route } from '../models/types';
import { redirect } from '../http-utils';

const routes: Route[] = [
	{
		type: 'POST',
		url: /\/signin/,
		method: sendSignInEmail,
	},
	{
		type: 'POST',
		url: /\/signin$/,
		format: 'text/html',
		method: displaySigninEmailSent,
	},
	{
		type: 'GET',
		url: /\/signout$/,
		method: signout,
	},
	{
		type: 'GET',
		url: /\/signout$/,
		format: 'text/html',
		method: redirect.backToHome,
	},
	{
		type: 'GET',
		url: /\/signin\/{{token:string}}\/?/,
		method: completeSignInEmail,
	},
	{
		type: 'GET',
		url: /\/signin\/{{token:string}}\/?/,
		format: 'text/html',
		method: redirect.backToHome,
	},
];

export default routes;
