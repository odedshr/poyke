import { IncomingMessage } from 'http';
import { encode, decode } from '../encryption';
import { Unauthorized } from '../../shared/Errors';
import { getServerIPs, parseCookies } from '../http-utils';

export interface AuthToken {
	localIP: string;
	serverIP: string;
	created?: Date;
	expires?: Date;
	userId?: string;
}

export async function sendSignInEmail(
	email: string,
	authToken: AuthToken,
	getUserIdByEmail: (email: string) => string
) {
	authToken.userId = await getUserIdByEmail(email);
	authToken.created = new Date();

	return new Promise((resolve) => {
		//TODO: should get sendMail() function and send the mail
		console.log({ email, token: encode(JSON.stringify(authToken)) });
		resolve({ action: 'signin', response: 'success' });
	});
}

export function completeSignInEmail(token: string) {
	return {
		response: 'get-signin-token',
		_headers: {
			'Set-Cookie': `__Secure-authorization=${token}; Secure; Path=/; Max-Age=2592000`,
		},
	};
}

export function getAuthUser(req: IncomingMessage): Promise<string> {
	return new Promise<string>((resolve, reject) => {
		const tokenFromRequest = getAuthTokenFromRequest(req);
		let value: any;
		// not authorized
		if (!tokenFromRequest) {
			return reject(new Unauthorized());
		}

		// bad auth code
		try {
			value = JSON.parse(decode(tokenFromRequest));
		} catch (err) {
			return reject(new Unauthorized());
		}

		// auth expired
		if (value.expires instanceof Date && value.expires < new Date()) {
			return reject(new Unauthorized());
		}
		const token = getAuthToken(req);

		// wrong auth code
		if (token.localIP !== value.localIP || token.serverIP !== value.serverIP) {
			throw new Unauthorized();
		}

		resolve(value.userId);
	});
}

function getAuthTokenFromRequest(req: IncomingMessage): string {
	return req.headers.authorization || parseCookies(req)['__Secure-authorization'];
}

export function getAuthToken(req: IncomingMessage): AuthToken {
	const headers = req.headers['x-forwarded-for'];
	return {
		localIP: (Array.isArray(headers) ? headers.join(';') : headers) || req.connection.remoteAddress,
		serverIP: getServerIPs().join(';'),
	};
}

export async function getOptionalUser(req: IncomingMessage): Promise<string | boolean> {
	return new Promise<string | boolean>((resolve) => resolve(getAuthUser(req).catch(() => false)));
}

export async function signout() {
	return {
		response: 'signed-out',
		_headers: {
			'Set-Cookie': `__Secure-authorization=; Secure; Path=/; Max-Age=0`,
		},
	};
}
