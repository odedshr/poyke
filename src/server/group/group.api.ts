import { Group, groupSpec, Variables, ModelSpec, Dictionary } from '../models/export';
import { User, Membership, membershipSpec } from '../models/export';
import { SaveFailed, MissingInput } from '../../shared/Errors';
import { getUser } from '../user/user.api';

export async function addGroup(
	authUser: string,
	group: Group = { name: '' },
	name: string,
	description: string,
	getById: (modelSpec: ModelSpec, id: string) => Promise<any>,
	getSetted: (dataObject: Dictionary, modelSpec: ModelSpec) => Promise<any>
) {
	const { user } = await getUser(authUser, getById);

	if (!user || !user.defaultAlias) {
		return new MissingInput('defaultAlias');
	}

	const sourceGroup: Dictionary = {
		...group,
		founderId: authUser,
		name: name || group.name,
		description: description || group.description,
	};
	if (!sourceGroup.name) {
		throw new MissingInput('group.name');
	}

	const dbGroup = await getSetted(sourceGroup, groupSpec);
	if (!dbGroup || !dbGroup.id) {
		return { error: new SaveFailed('group', group, dbGroup) };
	}

	const sourceMembership: Membership = {
		userId: authUser,
		groupId: dbGroup.id,
		alias: user.defaultAlias,
	};

	const membership = await getSetted(<Variables>(<unknown>sourceMembership), membershipSpec);

	return { group: dbGroup, membership };
}

export async function getGroups(optionalUser: string, get: Function) {
	const response = await get('SELECT * FROM groups');
	return { response };
}

export async function getGroup(optionalUser: string, getById: Function, id: string) {
	const group = await getById(groupSpec, id);
	return { group };
}
