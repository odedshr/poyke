import { DOMParser } from 'xmldom';
import { Dictionary } from '../models/types';
import { Group } from '../models/export';
import GroupPage from './group-page.template';
import CommonPage from '../pages/common.template';

const domParser = new DOMParser();

export async function getGroupPage(group: Group): Promise<Dictionary> {
	return new Promise<Dictionary>((resolve) =>
		resolve({
			_string: new CommonPage(
				{ title: group.name, content: new GroupPage({ group }, domParser) },
				domParser
			).toString(),
		})
	);
}
