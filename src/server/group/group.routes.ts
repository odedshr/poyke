import { responseStatus, Route } from '../models/types';
import { addGroup, getGroups, getGroup } from './group.api';
import { getGroupPage } from './group.ui';
import { Group } from '../models/export';

const routes: Route[] = [
	{
		type: 'GET',
		url: /^\/group\/{{id}}\/?$/,
		method: getGroup,
	},
	{
		type: 'GET',
		url: /^\/group\/{{id}}\/?$/,
		format: 'text/html',
		method: getGroupPage,
	},
	{
		type: 'POST',
		url: /^\/group\/?$/,
		method: addGroup,
	},
	{
		type: 'POST',
		url: /^\/group\/?$/,
		format: 'text/html',
		method(group: Group, error: Error) {
			console.log({ group, error });
			//TODO: redirect to error page if error;
			return { _redirect: `/group/${group.id}`, status: responseStatus.redirect };
		},
	},
	{
		type: 'GET',
		url: /^\/group\/?$/,
		method: getGroups,
	},
];

export default routes;
