import { existsSync, readFileSync } from 'fs';
import { IncomingMessage, RequestListener, ServerResponse } from 'http';
import { Server, createServer } from 'https';

import { Injectable, processRequest } from './requestProcessor';
import { write } from './responseWriter';
import { DetailedError, NotFound, Unauthorized } from '../shared/Errors';
import { responseStatus, Route } from './models/types';
import { RouterFunction } from './router';

interface SSLDetails {
	key: string;
	cert: string;
}

export class WebServer {
	server: Server;
	rootFolder: string;
	defaultFile: string = 'index.html';
	port: number;
	getRoute: RouterFunction;
	injectables: { [key: string]: Injectable } = {};

	constructor(
		port: number,
		rootFolder: string,
		defaultFile: string,
		getRoute: RouterFunction,
		injectables: { [key: string]: Injectable }
	) {
		this.rootFolder = rootFolder;
		this.defaultFile = defaultFile;
		this.port = port;
		this.getRoute = getRoute;
		this.injectables = injectables;
	}

	private async createServer(
		rootFolder: string,
		defaultFile: string,
		injectables: { [key: string]: Injectable }
	): Promise<Server> {
		const sslDetails: SSLDetails = await this.getExistingOrNewSSLDetails();

		return createServer(sslDetails, this.getHandleRequest(rootFolder, defaultFile, injectables));
	}

	async start(port: number = this.port) {
		if (!this.server) {
			this.server = await this.createServer(`${this.rootFolder}/www`, this.defaultFile, this.injectables);
		}
		this.server.listen(port);
	}

	stop() {
		this.server.close();
	}

	private getHandleRequest(
		wwwFolder: string,
		defaultFile: string,
		injectables: { [key: string]: Injectable }
	): RequestListener {
		return async function handleRequest(request: IncomingMessage, response: ServerResponse) {
			const { url } = request;

			try {
				if (!serveFile(wwwFolder, url, defaultFile, response)) {
					const startTime = new Date();
					const content = await processRequest(
						this.getRoute(request.method.toLocaleLowerCase(), request.url).filter(
							(route: Route) => !route.format || request.headers.accept.match(route.format)
						),
						request,
						response,
						injectables
					).catch((err) => err);

					if (content instanceof DetailedError) {
						switch (content.status) {
							case NotFound.status:
								return handleUnknownUrl(wwwFolder, request, response);
							case Unauthorized.status:
								return handleUnauthorized(wwwFolder, content, response);
							default:
								console.error('unhandled error', content);
						}
					} else {
						write(response, startTime, content);
					}
				}
			} catch (err) {
				response.writeHead(err.statusCode);
				response.end(err.message);
			}
		};
	}

	private async getExistingOrNewSSLDetails(): Promise<SSLDetails> {
		const errMessage = `
CRITICAL ERROR: SSL DETAILS ARE MISSING.
Run the command \`openssl req -nodes -new -x509 -keyout ${this.rootFolder}/var/server.key -out ${this.rootFolder}/var/server.cert\`
and paste in chrome \`chrome://flags/#allow-insecure-localhost\`
`;
		const key = `${this.rootFolder}/var/server.key`;
		const cert = `${this.rootFolder}/var/server.cert`;
		throwIfFileNotExist(key, errMessage);
		throwIfFileNotExist(cert, errMessage);

		return new Promise((resolve) =>
			resolve({
				key: readFileSync(key, 'utf8'),
				cert: readFileSync(cert, 'utf8'),
			})
		);
	}
}

function throwIfFileNotExist(file: string, errMessage: string): void {
	if (!existsSync(file)) {
		console.error(errMessage);
		throw new NotFound('ssl', file);
	}
}

function serveFile(
	root: string,
	url: string,
	defaultFile: string,
	response: ServerResponse,
	statusCode = responseStatus.ok
) {
	if ('/' === url) {
		url = `/${defaultFile}`;
	}

	const filePath = `${root}${url}`;

	if (!existsSync(filePath)) {
		return false;
	}

	const data = readFileSync(filePath);
	response.writeHead(statusCode);
	response.end(data);

	return true;
}

function handleUnknownUrl(root: string, request: IncomingMessage, response: ServerResponse) {
	console.error('404 not Found', request.url);
	serveFile(root, '/404.html', '', response, responseStatus.ok);
}

function handleUnauthorized(root: string, err: Unauthorized, response: ServerResponse) {
	console.error('401 Unauthorised', err.message);
	8;
	serveFile(root, '/401.html', '', response, err.status);
}
