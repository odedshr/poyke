import { existsSync, mkdirSync } from 'fs';

export function verifyFolderExists(path: string) {
	let agg = '.';
	if (!existsSync(path)) {
		path.split('/').forEach(folder => {
			agg = `${agg}/${folder}`;
			if (folder !== '.' && !existsSync(agg)) {
				mkdirSync(agg);
			}
		});
	}
}
