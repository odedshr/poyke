import { ModelSpec } from './types';

export type GroupStatus = 'active' | 'suspended' | 'archive';
export type GroupType = 'public' | 'exclusive' | 'secret';
export type Gender = 'n/a' | 'female' | 'male';

export interface Group {
	id?: string;
	name: string;
	description?: string;
	status?: GroupStatus;
	type?: GroupType;
	founderId?: string;
	hasImage?: boolean;
	isAnonymousAllowed?: boolean;
	postLength?: number;
	minAge?: number;
	maxAge?: number;
	gender?: Gender;
	members?: number;
	posts?: number;
	events?: number;
	score?: number;
	scoreDate?: Date;
	created?: Date;
	modified?: Date;
}

export const groupSpec: ModelSpec = {
	table: 'groups',
	columns: {
		id: { type: 'string' },
		name: { type: 'string' },
		description: { type: 'string' },
		status: { type: 'enum', values: ['active', 'suspended', 'archive'] },
		type: { type: 'enum', values: ['public', 'exclusive', 'secret'] },
		founderId: { type: 'string' },
		hasImage: { type: 'boolean' },
		isAnonymousAllowed: { type: 'number' },
		postLength: { type: 'number' },
		minAge: { type: 'number' },
		maxAge: { type: 'number' },
		gender: { type: 'enum', values: ['n/a', 'female', 'male'] },
		members: { type: 'number' },
		posts: { type: 'number' },
		events: { type: 'number' },
		score: { type: 'number' },
		scoreDate: { type: 'timestamp' },
		created: { type: 'timestamp' },
		modified: { type: 'timestamp' },
	},
};
