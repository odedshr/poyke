import { ModelSpec, Variables } from './types';
import { User } from './User';
import { Group } from './Group';

export type MembershipStatus =
	| 'invited'
	| 'requested'
	| 'declined'
	| 'rejected'
	| 'active'
	| 'unfit'
	| 'quit'
	| 'archived';

export interface Membership {
	id?: string;
	userId: string;
	user?: User;
	groupId: string;
	group?: Group;
	alias: string;
	status?: MembershipStatus;
	description?: string;
	hasImage?: boolean;
	birthDate?: Date;
	permissions?: Variables;
	penalties?: Variables;
	isModerator?: boolean;
	isPublic?: boolean;
	isRepresentative?: boolean;
	created?: Date;
	modified?: Date;
}

export const membershipSpec: ModelSpec = {
	table: 'memberships',
	columns: {
		id: { type: 'string' },
		userId: { type: 'string' },
		groupId: { type: 'string' },
		alias: { type: 'string' },
		status: {
			type: 'enum',
			values: ['invited', 'requested', 'declined', 'rejected', 'active', 'unfit', 'quit', 'archived'],
		},
		description: { type: 'string' },
		hasImage: { type: 'boolean' },
		birthDate: { type: 'timestamp' },
		permissions: { type: 'string' },
		penalties: { type: 'string' },
		isModerator: { type: 'boolean' },
		isPublic: { type: 'boolean' },
		isRepresentative: { type: 'boolean' },
		created: { type: 'timestamp' },
		modified: { type: 'timestamp' },
	},
};
