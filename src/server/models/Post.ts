import { ModelSpec } from './types';

export type PostStatus = 'starred' | 'published' | 'draft' | 'archived';

export interface Post {
	id?: string;
	parentId?: string;
	groupId?: string;
	authorId?: string;
	status?: PostStatus;
	content: string;
	follow: number;
	endorse: number;
	report: number;
	replies: number;
	score?: number;
	scoreDate?: Date;
	created?: Date;
	modified?: Date;
}

export const postSpec: ModelSpec = {
	table: 'posts',
	columns: {
		id: { type: 'string' },
		parentId: { type: 'string' },
		groupId: { type: 'string' },
		authorId: { type: 'string' },
		status: { type: 'enum', values: ['starred', 'published', 'draft', 'archived'] },
		content: { type: 'string' },
		follow: { type: 'number' },
		endorse: { type: 'number' },
		report: { type: 'number' },
		replies: { type: 'number' },
		score: { type: 'number' },
		scoreDate: { type: 'timestamp' },
		created: { type: 'timestamp' },
		modified: { type: 'timestamp' },
	},
};
