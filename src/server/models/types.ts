export type Dictionary = { [key: string]: any };
export type Variables = { [key: string]: string };

export const responseStatus = {
	ok: 200,
	redirect: 302,
	notFound: 404,
	systemError: 500,
};

export type RequestType = 'GET' | 'POST' | 'PUT' | 'DELETE';
export type Route = {
	type: RequestType;
	url: RegExp;
	method: Function;
	format?: 'text/html' | 'application/json';
	keys?: string[];
};

type ColumnType = 'boolean' | 'datetime' | 'enum' | 'number' | 'string' | 'timestamp';
export interface ColumnSpec {
	type?: ColumnType;
	values?: string[];
	isRequired?: boolean;
}

export type ModelSpec = { table: string; columns: { [key: string]: ColumnSpec } };
