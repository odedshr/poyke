import { ModelSpec } from './types';

export interface User {
	id: string;
	email?: string;
	defaultAlias?: string;
}

export const userSpec: ModelSpec = {
	table: 'users',
	columns: {
		id: { type: 'string' },
		email: { type: 'string' },
		defaultAlias: { type: 'string' },
	},
};
