const strings = {
	en: {
		appTitle: 'Poyke',
		addGroupTitle: 'Add Group',
		addGroupCTA: 'Add',
	},
};
const defaultLanguage = 'en';

export function getString(key: string, language = defaultLanguage): string {
	if (!strings[language]) {
		language = defaultLanguage;
	}

	return strings[language][key] || key;
}

// languages is a string that looks like `en-US,en;q=0.9,he;q=0.8`
export function getBestlanguage(languages: string): string {
	const available = Object.keys(strings);

	return (
		(languages || '')
			.split(',')
			.map((language) => language.split(';')[0])
			.find((language) => available.indexOf(language) > -1) || defaultLanguage
	);
}
