// rollup.config.js
import html from 'rollup-plugin-html-encoder';
import typescript from '@rollup/plugin-typescript';

export default {
	input: 'src/server/cli.ts',
	external: [
		'crypto',
		'dotenv',
		'fs',
		'http',
		'https',
		'jwt-simple',
		'os',
		'passport',
		'passport-local',
		'path',
		'readline',
		'sqlite3'
	],
	output: {
		file: 'dist/server.js',
		format: 'cjs'
	},
	plugins: [html({ include: '**/*.html' }), typescript()],
	//https://stackoverflow.com/questions/58593754/openlayers-6-rollupjs-recipe
	onwarn: function(warning, superOnWarn) {
		if (warning.code === 'THIS_IS_UNDEFINED') {
			return;
		}
		superOnWarn(warning);
	}
};
